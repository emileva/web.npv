﻿ko.validation.init({
    decorateInputElement: true,
    errorMessageClass: 'invalid-feedback',
    errorElementClass: 'is-invalid',
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null
}, true);

var model = {
    InitialValue: ko.observable().extend({ required: true, number: true, min: 0 }),
    Cashflow: ko.observable().extend({ required: true, number: true, min: 0 }),
    Years: ko.observable().extend({ required: true, number: true, min: 0 }),
    LowerBoundDiscountRate: ko.observable().extend({ required: true, number: true, min: 0 }),
    UpperBoundDiscountRate: ko.observable().extend({ required: true, number: true, min: 0 }),
    Increment: ko.observable().extend({ required: true, number: true, min: 0 })
};

var viewModel = function () {
    var me = this;

    me.Model = model;
    me.NetPresentValueConfig = ko.observable({
        InitialValue: 0,
        CashFlow: 0,
        Years: 0,
        LowerBoundDiscountRate: 0,
        UpperBoundDiscountRate: 0,
        Increment: 0,
        NetPresentValues: []
    });
    me.NetPresentValueConfigs = ko.observableArray();

    me.Calculate = onCalculate;
    me.GetNetPresentValueConfig = onGetNetPresentValueConfig;
    me.GetNetPresentValueConfigs = onGetNetPresentValueConfigs;
    me.Select = onSelect;

    me.GetNetPresentValueConfigs();
    me.errors = ko.validation.group(me.Model);
};

function onCalculate() {
    var me = this;

    if (me.errors().length === 0) {
        $.ajax({
            type: 'POST',
            url: 'home/calculate',
            data: me.Model,
            success: function (res) {
                if (res.Ok) {
                    me.GetNetPresentValueConfig(res.Data.Id);
                    me.GetNetPresentValueConfigs();
                }
                else {
                    console.log(res.Message);
                }

            },
            error: function (err) {
                console.log(err);
            }
        });
    }
    else {
        me.errors.showAllMessages();
    }
}

function onGetNetPresentValueConfig(id) {
    var me = this;

    $.ajax({
        type: 'GET',
        url: 'home/GetNetPresentValueConfig/' + id,
        success: function (res) {
            me.NetPresentValueConfig(res);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function onGetNetPresentValueConfigs() {
    var me = this;

    $.ajax({
        type: 'GET',
        url: 'home/GetNetPresentValueConfigs',
        success: function (res) {
            me.NetPresentValueConfigs(JSON.parse(res));
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function onSelect(data, event) {
    var selector = event.target.parentElement;
    var allRows = selector.parentElement.children;

    $(allRows).removeClass("selected");
    $(selector).addClass("selected");

    this.GetNetPresentValueConfig(data.id);
}

ko.applyBindings(new viewModel(), document.getElementsByTagName('body')[0]);