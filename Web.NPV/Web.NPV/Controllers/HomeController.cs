﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Web.NPV.Models;

namespace Web.NPV.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApiResultModel _apiResultModel;

        public HomeController()
        {
            _apiResultModel = new ApiResultModel();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetNetPresentValueConfig(int id)
        {
            var url = "api/netpresentvalueconfigs/" + id + "/netpresentvalues";

            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    var model = JsonConvert.DeserializeObject<NetPresentValueConfigModel>(content);
                    return Json(model, JsonRequestBehavior.AllowGet);
                }

                return Json("An error occured: " + content, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetNetPresentValueConfigs()
        {
            var url = "api/netpresentvalueconfigs";

            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    return Json(content, JsonRequestBehavior.AllowGet);
                }

                return Json("An error occured: " + content, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> Calculate(NetPresentValueConfigModel postData)
        {
            var netPresentValueConfig = new NetPresentValueConfigModel
            {
                Increment = postData.Increment,
                LowerBoundDiscountRate = postData.LowerBoundDiscountRate,
                UpperBoundDiscountRate = postData.UpperBoundDiscountRate,
                InitialValue = postData.InitialValue,
                Years = postData.Years,
                CashFlow = postData.CashFlow
            };

            using (HttpResponseMessage response = await ApiHelper.ApiClient.PostAsJsonAsync("/api/netpresentvalueconfigs", netPresentValueConfig))
            {
                if (response.IsSuccessStatusCode)
                {
                    _apiResultModel.Ok = true;
                    _apiResultModel.Data = JsonConvert.DeserializeObject<NetPresentValueConfigModel>(response.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    _apiResultModel.Message = "An error occured, please try again.";
                }

                return Json(_apiResultModel, JsonRequestBehavior.AllowGet);
            } 
        }
    }
}