﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.NPV.Models
{
    public class NetPresentValueConfigModel
    {
        public int Id { get; set; }
        public double LowerBoundDiscountRate { get; set; }
        public double UpperBoundDiscountRate { get; set; }
        public double Increment { get; set; }
        public decimal InitialValue { get; set; }
        public int Years { get; set; }
        public decimal CashFlow { get; set; }
        public List<NetPresentValueModel> NetPresentValues { get; set; }
    }
}