﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.NPV.Models
{
    public class ApiResultModel
    {
        public bool Ok { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }

    }
}