﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.NPV.Models
{
    public class NetPresentValueModel
    {
        public int Id { get; set; }
        public double DiscountRate { get; set; }
        public decimal Amount { get; set; }
    }
}